package net.stuchl4n3k.transparentaccounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application main class.
 *
 * <h2>Configured features</h2>
 * <ul>
 *     <li>Spring Web MVC is enabled and using embedded Tomcat 8 server (packaged in the runnable JAR).</li>
 *     <li>Caching is enabled and configured to proxy target classes.</li>
 *     <li>Thymeleaf templates support is enabled (see {@code resources/templates}).</li>
 *     <li>Basic Auth security is auto-configured using credentials in {@code application.properties}.</li>
 *     <li>AOP-based REST controller benchmarking logs execution time of each public method.</li>
 * </ul>
 *
 * @author petr.stuchlik
 * @since 2017-06-25
 */
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
