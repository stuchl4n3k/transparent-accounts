package net.stuchl4n3k.transparentaccounts.integration.rest;

import java.io.File;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import net.stuchl4n3k.transparentaccounts.service.BankAccountService;
import net.stuchl4n3k.transparentaccounts.service.BankTransactionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author petr.stuchlik
 * @since 2017-07-14
 */
@RestController
@RequestMapping("/transactions")
@RequiredArgsConstructor
@Slf4j
public class TransactionsController {

    private final BankAccountService bankAccountService;
    private final BankTransactionService bankTransactionService;

    @RequestMapping
    public List<BankTransaction> getAll() {
        List<BankAccount> accounts = bankAccountService.loadAllForPartyCampaigns();
        List<BankTransaction> transactions = bankTransactionService.loadAll(accounts);

        Collections.sort(transactions);
        Collections.reverse(transactions);

        File transactionsExportFile = new File("transactions.csv");
        if (transactionsExportFile.exists()) {
            transactionsExportFile.delete();
        }

        return transactions;
    }
}
