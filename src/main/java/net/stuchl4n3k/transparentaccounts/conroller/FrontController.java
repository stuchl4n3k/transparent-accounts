package net.stuchl4n3k.transparentaccounts.conroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author petr.stuchlik
 * @since 2017-07-14
 */
@Controller
@RequestMapping("/")
public class FrontController {

    @GetMapping
    public String viewTransactions() {
        return "transactions";
    }
}
