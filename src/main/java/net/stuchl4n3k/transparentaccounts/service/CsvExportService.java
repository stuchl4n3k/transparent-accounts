package net.stuchl4n3k.transparentaccounts.service;

import com.univocity.parsers.common.processor.BeanWriterProcessor;
import com.univocity.parsers.csv.CsvFormat;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.stereotype.Service;

/**
 * @author petr.stuchlik
 * @since 2017-07-14
 */
@Slf4j
@Service
public class CsvExportService {

    private final CsvWriterSettings transactionWriterSettings = createCsvWriterSettings(BankTransaction.class);

    public void export(BankTransaction transaction, File destination) {
        try (FileWriter fileWriter = new FileWriter(destination, true)) {
            CsvWriter writer = new CsvWriter(fileWriter, transactionWriterSettings);
            if (destination.length() == 0) {
                writer.writeHeaders();
            }

            writer.processRecord(transaction);
        } catch (IOException e) {
            LOG.error("Could not write metadata to CSV file. The cause was:", e);
        }
    }

    @SuppressWarnings("unchecked")
    protected CsvWriterSettings createCsvWriterSettings(Class beanClass) {
        CsvWriterSettings settings = new CsvWriterSettings();

        // Custom format: use colon instead of default comma.
        CsvFormat format = new CsvFormat();
        format.setDelimiter(';');
        settings.setFormat(format);

        // Use bean-specific row writer processor.
        BeanWriterProcessor beanWriterProcessor = new BeanWriterProcessor<>(beanClass);
        settings.setRowWriterProcessor(beanWriterProcessor);

        return settings;
    }

}
