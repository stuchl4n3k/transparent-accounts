package net.stuchl4n3k.transparentaccounts.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import net.stuchl4n3k.transparentaccounts.dao.impl.account.PartyCampaignAccountDao;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author petr.stuchlik
 * @since 2017-06-26
 */
@Service
@CacheConfig(cacheNames = "accounts")
@RequiredArgsConstructor
public class BankAccountService {

    private final PartyCampaignAccountDao partyCampaignAccountDao;

    @Cacheable
    public List<BankAccount> loadAllForPartyCampaigns() {
        return partyCampaignAccountDao.loadAll();
    }
}
