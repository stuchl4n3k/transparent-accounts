package net.stuchl4n3k.transparentaccounts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import net.stuchl4n3k.transparentaccounts.dao.BankTransactionDao;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author petr.stuchlik
 * @since 2017-06-26
 */
@Service
@CacheConfig(cacheNames = "transactions")
@RequiredArgsConstructor
public class BankTransactionService {

    private final Map<Bank, BankTransactionDao> transactionDaoContainer;

    @Autowired
    private BankTransactionService selfProxy;

    public List<BankTransaction> loadAll(List<BankAccount> accounts) {
        List<BankTransaction> transactions = new ArrayList<>();

        for (BankAccount account : accounts) {
            transactions.addAll(selfProxy.loadAll(account));
        }

        return transactions;
    }

    @Cacheable
    public List<BankTransaction> loadAll(BankAccount account) {
        BankTransactionDao bankTransactionDao = transactionDaoContainer.get(account.getBank());
        if (bankTransactionDao == null) {
            return new ArrayList<>();
        }
        return bankTransactionDao.loadAllByAccount(account);
    }
}
