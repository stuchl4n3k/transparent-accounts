package net.stuchl4n3k.transparentaccounts.config;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import net.stuchl4n3k.transparentaccounts.dao.BankTransactionDao;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author petr.stuchlik
 * @since 2017-06-27
 */
@Configuration
public class DaoConfig {

    /**
     * Container of {@link BankTransactionDao}s with each DAO keyed by its supported {@link Bank}.
     */
    @Bean
    public Map<Bank, BankTransactionDao> transactionDaoContainer(Set<BankTransactionDao> transactionDaos) {
        return transactionDaos.stream()
                .collect(Collectors.toMap(BankTransactionDao::getSupportedBank, item -> item));
    }
}
