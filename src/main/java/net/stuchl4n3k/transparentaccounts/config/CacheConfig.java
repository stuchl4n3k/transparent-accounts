package net.stuchl4n3k.transparentaccounts.config;

import java.text.DateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author petr.stuchlik
 * @since 2017-07-15
 */
@Configuration
@EnableCaching(proxyTargetClass = true)
@EnableScheduling
@Slf4j
public class CacheConfig {

    private static final long DELAY_1_SECOND = 1000;
    private static final long DELAY_1_MINUTE = 60 * DELAY_1_SECOND;
    private static final long DELAY_1_HOUR = 60 * DELAY_1_MINUTE;

    @Autowired
    private CacheManager cacheManager;

    @PostConstruct
    public void logCacheManagerImplementation() {
        LOG.info("Using cache manager: " + cacheManager.getClass().getName());
    }

    @CacheEvict(allEntries = true, value = {"accounts", "transactions"})
    @Scheduled(fixedDelay = DELAY_1_HOUR, initialDelay = 5 * DELAY_1_SECOND)
    public void reportCacheEvict() {
        LOG.info("Flushed cache at {}.", DateFormat.getDateTimeInstance().format(new Date()));
    }
}
