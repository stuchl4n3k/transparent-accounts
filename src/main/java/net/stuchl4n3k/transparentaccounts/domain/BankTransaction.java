package net.stuchl4n3k.transparentaccounts.domain;

import com.univocity.parsers.annotations.Convert;
import com.univocity.parsers.annotations.Format;
import com.univocity.parsers.annotations.Parsed;
import com.univocity.parsers.conversions.Conversion;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Data;

/**
 * @author petr.stuchlik
 * @since 2017-06-24
 */
@Data
public class BankTransaction implements Comparable<BankTransaction> {

    private static Comparator<Date> nullSafeDateComparator = Comparator.nullsFirst(Date::compareTo);

    @Parsed(field = "DATE")
    @Format(formats = "YYYY-MM-dd")
    private final Date dateCreated;

    @Parsed(field = "FROM")
    @Convert(conversionClass = BankAccountConverter.class)
    private final BankAccount from;

    @Parsed(field = "TO")
    @Convert(conversionClass = BankAccountConverter.class)
    private final BankAccount to;

    @Parsed(field = "AMOUNT")
    @Format(formats = "#,##0.00", options = {"decimalSeparator=.", "groupingSeparator= "})
    private final Float amount;

    @Parsed(field = "CCY")
    private final Currency currency;

    @Parsed(field = "VS")
    private String variableSymbol;

    @Parsed(field = "CS")
    private String constantSymbol;

    @Parsed(field = "SS")
    private String specificSymbol;

    @Parsed(field = "DESCRIPTION")
    private String description;

    @Parsed(field = "MESSAGE")
    private String messageForRecipient;

    @Override
    public int compareTo(BankTransaction o) {
        return nullSafeDateComparator.compare(this.getDateCreated(), o.getDateCreated());
    }

    public static class BankAccountConverter implements Conversion<String, BankAccount> {

        public BankAccountConverter(String[] args) {
            // N/A
        }

        @Override
        public BankAccount execute(String number) {
            throw new UnsupportedOperationException("Parsing BankAccount from String is not supported.");
        }

        @Override
        public String revert(BankAccount account) {
            String[] fields = new String[]{account.getNumber(), account.getName()};
            return Arrays.stream(fields)
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(", "));
        }
    }
}
