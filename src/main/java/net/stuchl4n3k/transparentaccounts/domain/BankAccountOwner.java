package net.stuchl4n3k.transparentaccounts.domain;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author petr.stuchlik
 * @since 2017-06-24
 */
@AllArgsConstructor
@Data
public class BankAccountOwner {

    private final String name;
    private String businessId;

    public String toString() {
        String[] fields = new String[]{
                name,
                businessId,
        };

        return "BankAccountOwner(" + Arrays.stream(fields)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(", "))
                + ")";
    }
}
