package net.stuchl4n3k.transparentaccounts.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeration of all supported banks.
 *
 * @author petr.stuchlik
 * @since 2017-06-24
 */
@RequiredArgsConstructor
@Getter
public enum Bank {

    KB("0100"),
    MONETA("0600"),
    CSAS("0800"),
    FIO("2010"),
    UNKNOWN(null);

    private final String code;

    public boolean isSupported() {
        return this != UNKNOWN;
    }

    public static Collection<Bank> getSupported() {
        return Arrays.stream(values()).filter(Bank::isSupported).collect(Collectors.toList());
    }

    public static Bank getByAccountNumber(String accountNumber) {
        return getByCode(findCodeInAccountNumber(accountNumber));
    }

    public static Bank getByCode(String code) {
        return Arrays.stream(values())
                .filter(bank -> Objects.equals(bank.code, code))
                .findFirst()
                .orElse(UNKNOWN);
    }

    protected static String findCodeInAccountNumber(String accountNumber) {
        return accountNumber.substring(accountNumber.lastIndexOf('/') + 1);
    }

    public String toString() {
        return "Bank(name=" + name() + ", code=" + this.getCode() + ")";
    }
}
