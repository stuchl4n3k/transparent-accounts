package net.stuchl4n3k.transparentaccounts.domain;

import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author petr.stuchlik
 * @since 2017-06-24
 */
@Data
@EqualsAndHashCode(of = {"number", "name"})
public class BankAccount {

    private final String number;
    private final Bank bank;

    private String name;
    private BankAccountOwner owner;
    private URL statementUrl;
    private Date dateCreated;

    public BankAccount(String number) {
        this.number = number;
        if (number != null) {
            this.bank = Bank.getByAccountNumber(number);
        } else {
            this.bank = Bank.UNKNOWN;
        }
    }

    public String toString() {
        String[] fields = new String[]{
                number,
                name,
        };

        return "BankAccount(" + Arrays.stream(fields)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(", "))
                + ")";
    }
}
