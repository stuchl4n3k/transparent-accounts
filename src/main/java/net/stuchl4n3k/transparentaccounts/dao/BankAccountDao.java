package net.stuchl4n3k.transparentaccounts.dao;

import java.util.List;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;

/**
 * @author petr.stuchlik
 * @since 2017-06-24
 */
public interface BankAccountDao {

    List<BankAccount> loadAll();
}
