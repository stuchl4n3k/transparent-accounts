package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import jodd.jerry.Jerry;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.stereotype.Repository;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
@Repository
@Slf4j
public class KbTransactionDao extends AbstractJerryTransactionDao {

    @Override
    public Bank getSupportedBank() {
        return Bank.KB;
    }

    @Override
    protected String getTransactionItemsSelector() {
        return ".transaction-table > tbody > tr";
    }

    @Override
    protected String getNextPageLinkSelector() {
        return ".pagination > .next > a";
    }

    @Override
    protected BankTransaction parseTransaction(BankAccount accountFrom, Jerry doc) {
        Date dateCreated;
        try {
            dateCreated = parseDate(doc, "td:nth-child(1)");
        } catch (ParseException e) {
            LOG.warn("Given transaction date format is invalid. The cause was:", e);
            return null;
        }

        String counterPartyAccountName = parseText(doc, "td:nth-child(2) table tr:nth-child(2) td");
        String counterPartyAccountNumber = parseText(doc, "td:nth-child(2) table tr:nth-child(3) td");
        if (!validateBankAccountNumber(counterPartyAccountNumber)) {
            LOG.warn("Bank account number '{}' is invalid for account name '{}'.", counterPartyAccountNumber, counterPartyAccountName);
            return null;
        }

        String variableSymbol = parseText(doc, "td:nth-child(3) table tr:nth-child(1) td");
        String constantSymbol = parseText(doc, "td:nth-child(3) table tr:nth-child(2) td");
        String specificSymbol = parseText(doc, "td:nth-child(3) table tr:nth-child(3) td");
        String description = parseText(doc, "td:nth-child(2) table tr:nth-child(1) td");
        String messageForRecipient = parseText(doc, "td:nth-child(2) table tr:nth-child(4) td");
        String currencyAmountString = parseText(doc, "td:nth-child(4)");
        float amount = parseAmount(currencyAmountString);

        BankAccount accountTo = new BankAccount(counterPartyAccountNumber);
        accountTo.setName(counterPartyAccountName);

        BankTransaction transaction = new BankTransaction(dateCreated, accountFrom, accountTo, amount, Currency.getInstance("CZK"));
        transaction.setDescription(description);
        transaction.setMessageForRecipient(messageForRecipient);
        transaction.setVariableSymbol(variableSymbol);
        transaction.setConstantSymbol(constantSymbol);
        transaction.setSpecificSymbol(specificSymbol);

        return transaction;
    }
}
