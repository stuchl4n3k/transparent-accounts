package net.stuchl4n3k.transparentaccounts.dao.impl.account;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import jodd.jerry.Jerry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.dao.BankAccountDao;
import net.stuchl4n3k.transparentaccounts.dao.impl.AbstractDao;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankAccountOwner;
import org.springframework.stereotype.Repository;

/**
 * @author petr.stuchlik
 * @since 2017-06-24
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class PartyCampaignAccountDao extends AbstractDao implements BankAccountDao {

    protected static final String FEED_URL = "https://registrace.udhpsh.cz/seznam/ofzvu/2017ps";

    @Override
    public List<BankAccount> loadAll() {
        String feedContent;
        try {
            feedContent = loadContent(FEED_URL);
        } catch (IOException e) {
            LOG.error("Could not load bank account feed content from URL '{}'. The cause was:", FEED_URL, e);
            return new ArrayList<>();
        }

        return parseAccounts(feedContent);
    }

    protected List<BankAccount> parseAccounts(String feedContent) {
        List<BankAccount> accounts = new ArrayList<>();

        Jerry doc = Jerry.jerry(feedContent);

        // Iterate over rows, skipping table header.
        Jerry rows = doc.$(".container .table tr:not(:first-child)");
        rows.each((jerry, i) -> {
            BankAccount account = parseAccount(jerry.html());
            if (account != null) {
                accounts.add(account);
            }
            return true;
        });

        return accounts;
    }

    protected BankAccount parseAccount(String accountContent) {
        Jerry doc = Jerry.jerry(accountContent);

        String name = parseText(doc, ":nth-child(1)");
        String businessId = parseText(doc, ":nth-child(2)");
        String number = parseText(doc, ":nth-child(3) a");
        String statementUrl = doc.$(":nth-child(3) a").attr("href");

        if (!validateBankAccountNumber(number)) {
            LOG.warn("Bank account number '{}' is invalid for account name '{}'.", number, name);

            if ("00409171".equals(businessId)) {
                // Special tweak for CSSD, since they don't publish their transparent account number (lolz)
                // and we need at least a bank code to use correct transaction DAO. Screw them and such!
                number = "?/2010";
            } else {
                return null;
            }
        }

        BankAccount account = new BankAccount(number);
        account.setName(name);
        account.setOwner(new BankAccountOwner(name, businessId));
        try {
            account.setStatementUrl(new URL(statementUrl));
        } catch (MalformedURLException e) {
            LOG.warn("Bank account number '{}' contains invalid statement URL '{}'.", number, statementUrl);
        }

        return account;
    }
}
