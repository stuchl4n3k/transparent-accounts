package net.stuchl4n3k.transparentaccounts.dao.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import jodd.jerry.Jerry;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
public abstract class AbstractDao {

    protected static final String TX_DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    protected static final String TX_CURRENCY_PATTERN = "\\p{Sc}";

    @Autowired
    private RestTemplate restTemplate;

    protected String loadContent(String url) throws IOException {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0");
        requestHeaders.add("Accept-Language", "en-US,en;q=0.5");

        HttpEntity<String> requestEntity = new HttpEntity<>("", requestHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
        return responseEntity.getBody();
    }

    protected String parseText(Jerry content, String selector) {
        String text = content.$(selector).get(0).getTextContent();
        return cleanText(text);
    }

    protected Date parseDate(Jerry content, String selector) throws ParseException {
        return parseDate(parseText(content, selector));
    }

    protected String cleanText(String text) {
        text = text.trim();
        text = text.replaceAll("\\s+", " ");
        if (text.length() == 0) {
            return null;
        } else if (text.equals("-") || text.equals("–")) {
            return null;
        }
        return text;
    }

    protected String cleanHtml(String html) {
        return new HtmlToPlainText().getPlainText(Jsoup.parse(html));
    }

    protected Date parseDate(String dateString) throws ParseException {
        dateString = dateString.replaceAll("[^0-9.]", "");
        SimpleDateFormat transactionDateFormat = new SimpleDateFormat(TX_DATE_FORMAT_PATTERN);
        return transactionDateFormat.parse(dateString);
    }

    protected float parseAmount(String humanReadableAmount) {
        return Float.parseFloat(humanReadableAmount
                .replaceAll("[^\\-0-9,.]", "")
                .replace(',', '.')
        );
    }

    protected boolean validateBankAccountNumber(String number) {
        return number.matches("[0-9\\-/]+");
    }
}
