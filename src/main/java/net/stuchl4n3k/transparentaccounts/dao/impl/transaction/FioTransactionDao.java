package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import jodd.jerry.Jerry;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.stereotype.Repository;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
@Repository
@Slf4j
public class FioTransactionDao extends AbstractJerryTransactionDao {

    @Override
    public Bank getSupportedBank() {
        return Bank.FIO;
    }

    @Override
    protected String getTransactionItemsSelector() {
        return ".content > .table > tbody > tr";
    }

    @Override
    protected boolean supportsPaging() {
        return false;
    }

    @Override
    protected String getNextPageLinkSelector() {
        throw new UnsupportedOperationException("FIO bank does not support paging.");
    }

    @Override
    protected BankTransaction parseTransaction(BankAccount accountFrom, Jerry doc) {
        Date dateCreated;
        try {
            dateCreated = parseDate(parseText(doc, "td:nth-child(1)"));
        } catch (ParseException e) {
            LOG.warn("Given transaction date format is invalid. The cause was:", e);
            return null;
        }

        String currencyAmountString = parseText(doc, "td:nth-child(2)");
        String counterPartyAccountName = parseText(doc, "td:nth-child(4)");
        String messageForRecipient = parseText(doc, "td:nth-child(5)");
        String constantSymbol = parseText(doc, "td:nth-child(6)");
        String variableSymbol = parseText(doc, "td:nth-child(7)");
        String specificSymbol = parseText(doc, "td:nth-child(8)");
        String description = parseText(doc, "td:nth-child(9)");

        float amount = parseAmount(currencyAmountString);

        BankAccount accountTo = new BankAccount(null);
        accountTo.setName(counterPartyAccountName);

        BankTransaction transaction = new BankTransaction(dateCreated, accountFrom, accountTo, amount, Currency.getInstance("CZK"));
        transaction.setDescription(description);
        transaction.setMessageForRecipient(messageForRecipient);
        transaction.setVariableSymbol(variableSymbol);
        transaction.setConstantSymbol(constantSymbol);
        transaction.setSpecificSymbol(specificSymbol);

        return transaction;
    }

    @Override
    protected String createStatementUrl(BankAccount bankAccount) {
        // FIO does not support paging, instead use from/to date filter -> take everything since 2016.
        return super.createStatementUrl(bankAccount) + "&f=01.01.2016";
    }
}
