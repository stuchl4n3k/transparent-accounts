package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import jodd.jerry.Jerry;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.stereotype.Repository;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
@Repository
@Slf4j
public class CsasTransactionDao extends AbstractJerryTransactionDao {

    @Override
    public Bank getSupportedBank() {
        return Bank.CSAS;
    }

    @Override
    protected String getTransactionItemsSelector() {
        return "#transactionTable > tbody > tr";
    }

    @Override
    protected String getNextPageLinkSelector() {
        return ".transparentAccount > .table > .pagelinks > a:contains('Další')";
    }

    @Override
    protected BankTransaction parseTransaction(BankAccount accountFrom, Jerry doc) {
        Date dateCreated;
        try {
            dateCreated = parseDate(parseText(doc, "td:nth-child(1)"));
        } catch (ParseException e) {
            LOG.warn("Given transaction date format is invalid. The cause was:", e);
            return null;
        }

        String counterPartyAccountName = null;
        String description = "";
        String counterPartyNameAndDescription = parseText(doc, "td:nth-child(2)");
        if (counterPartyNameAndDescription != null) {
            String[] counterPartyNameAndDescriptionStrings = counterPartyNameAndDescription.split("\n");

            if (counterPartyNameAndDescriptionStrings.length > 0) {
                counterPartyAccountName = cleanText(counterPartyNameAndDescriptionStrings[0]);
            }
            if (counterPartyNameAndDescriptionStrings.length > 1) {
                description = cleanText(counterPartyNameAndDescriptionStrings[1]);
            }
        }

        description += "\n" + parseText(doc, "td:nth-child(3)");
        description = cleanText(description);

        // VS, CS and SS are messed up together in this feed: don't even bother parsing them.
        // String constantSymbol = parseText(doc, "td:nth-child(4)");
        // String variableSymbol = parseText(doc, "td:nth-child(4)");
        // String specificSymbol = parseText(doc, "td:nth-child(4)");

        String currencyAmountString = parseText(doc, "td:nth-child(5)");
        String messageForRecipient = parseText(doc, "td:nth-child(6)");

        float amount = parseAmount(currencyAmountString);

        BankAccount accountTo = new BankAccount(null);
        accountTo.setName(counterPartyAccountName);

        BankTransaction transaction = new BankTransaction(dateCreated, accountFrom, accountTo, amount, Currency.getInstance("CZK"));
        transaction.setDescription(description);
        transaction.setMessageForRecipient(messageForRecipient);


        return transaction;
    }
}
