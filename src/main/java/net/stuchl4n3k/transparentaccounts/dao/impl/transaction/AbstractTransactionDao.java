package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.dao.BankTransactionDao;
import net.stuchl4n3k.transparentaccounts.dao.impl.AbstractDao;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;

/**
 * @author petr.stuchlik
 * @since 2017-06-26
 */
@Slf4j
public abstract class AbstractTransactionDao extends AbstractDao implements BankTransactionDao {

    @Override
    public List<BankTransaction> loadAllByAccount(BankAccount account) {
        return loadAllByAccount(account, createStatementUrl(account));
    }

    protected List<BankTransaction> loadAllByAccount(BankAccount account, String url) {
        String content;
        try {
            content = loadContent(url);
        } catch (IOException e) {
            LOG.error("Could not load bank transactions content from URL '{}'. The cause was:", url, e);
            return new ArrayList<>();
        }

        List<BankTransaction> transactions = new ArrayList<>();
        transactions.addAll(parseStatements(account, content));

        // Check if there is a next page.
        if (supportsPaging()) {
            Optional<String> nextPageUrl = parseNextPageUrl(account, content);
            nextPageUrl.ifPresent(u -> transactions.addAll(loadAllByAccount(account, u)));
        }

        return transactions;
    }

    protected String createStatementUrl(BankAccount bankAccount) {
        return bankAccount.getStatementUrl().toString();
    }

    protected boolean supportsPaging() {
        return true;
    }

    protected abstract Optional<String> parseNextPageUrl(BankAccount account, String content);

    protected abstract List<BankTransaction> parseStatements(BankAccount account, String content);
}
