package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import jodd.jerry.Jerry;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.stereotype.Repository;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
@Repository
@Slf4j
public class MonetaTransactionDao extends AbstractJerryTransactionDao {

    @Override
    public Bank getSupportedBank() {
        return Bank.MONETA;
    }

    @Override
    protected String getTransactionItemsSelector() {
        return "#transparentAccountTable > tbody > tr";
    }

    @Override
    protected boolean supportsPaging() {
        return false;
    }

    @Override
    protected String getNextPageLinkSelector() {
        throw new UnsupportedOperationException("MONETA bank does not support paging.");
    }

    @Override
    protected BankTransaction parseTransaction(BankAccount accountFrom, Jerry doc) {
        Date dateCreated;
        try {
            dateCreated = parseDate(parseText(doc, "td:nth-child(1)"));
        } catch (ParseException e) {
            LOG.warn("Given transaction date format is invalid. The cause was:", e);
            return null;
        }

        String counterPartyAccountName = null;
        String description = null;
        String counterPartyNameAndDescription = parseText(doc, "td:nth-child(2)");
        if (counterPartyNameAndDescription != null) {
            String[] counterPartyNameAndDescriptionStrings = counterPartyNameAndDescription.split("\n");

            if (counterPartyNameAndDescriptionStrings.length > 0) {
                counterPartyAccountName = cleanText(counterPartyNameAndDescriptionStrings[0]);
            }
            if (counterPartyNameAndDescriptionStrings.length > 1) {
                description = cleanText(counterPartyNameAndDescriptionStrings[1]);
            }
        }

        String variableSymbol = parseText(doc, "td:nth-child(4)");
        String currencyAmountString = parseText(doc, "td:nth-child(5)");

        float amount = parseAmount(currencyAmountString);

        BankAccount accountTo = new BankAccount(null);
        accountTo.setName(counterPartyAccountName);

        BankTransaction transaction = new BankTransaction(dateCreated, accountFrom, accountTo, amount, Currency.getInstance("CZK"));
        transaction.setDescription(description);
        transaction.setVariableSymbol(variableSymbol);


        return transaction;
    }
}
