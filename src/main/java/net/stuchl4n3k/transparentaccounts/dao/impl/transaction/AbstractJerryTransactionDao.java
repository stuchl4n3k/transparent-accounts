package net.stuchl4n3k.transparentaccounts.dao.impl.transaction;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import jodd.jerry.Jerry;
import lombok.extern.slf4j.Slf4j;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.StringUtils;

/**
 * @author petr.stuchlik
 * @since 2017-06-27
 */
@Slf4j
public abstract class AbstractJerryTransactionDao extends AbstractTransactionDao {

    protected List<BankTransaction> parseStatements(BankAccount account, String content) {
        List<BankTransaction> transactions = new ArrayList<>();

        // Iterate over rows, skipping table header.
        Jerry doc = Jerry.jerry(content);
        Jerry rows = doc.$(getTransactionItemsSelector());
        rows.each((jerry, i) -> {
            BankTransaction transaction = parseTransaction(account, jerry);
            if (transaction != null) {
                transactions.add(transaction);
            }
            return true;
        });

        return transactions;
    }

    @Override
    protected Optional<String> parseNextPageUrl(BankAccount account, String content) {
        String nextPageLinkSelector = getNextPageLinkSelector();
        if (nextPageLinkSelector == null) {
            return Optional.empty();
        }

        Jerry doc = Jerry.jerry(content);
        Jerry nextPageLink = doc.$(nextPageLinkSelector);
        String href = nextPageLink.attr("href");
        if (StringUtils.hasText(href)) {
            String absoluteUrl;

            try {
                absoluteUrl = createAbsoluteUrl(account.getStatementUrl(), href);
            } catch (MalformedURLException e) {
                LOG.error("Could not convert a given URL '{}' to absolute form. The cause was:", href, e);
                return Optional.empty();
            }

            return Optional.of(absoluteUrl);
        }

        return Optional.empty();
    }

    protected abstract String getTransactionItemsSelector();

    protected abstract String getNextPageLinkSelector();

    protected abstract BankTransaction parseTransaction(BankAccount accountFrom, Jerry doc);

    private String createAbsoluteUrl(URL pageUrl, String href) throws MalformedURLException {
        if (!UrlUtils.isAbsoluteUrl(href)) {
            if (href.startsWith("/")) {
                return new URL(pageUrl.getProtocol(), pageUrl.getHost(), pageUrl.getPort(), href).toString();
            } else {
                return new URL(pageUrl.getProtocol(), pageUrl.getHost(), pageUrl.getPort(),
                        pageUrl.getFile() + "/" + href).toString();
            }
        }
        return href;
    }
}
