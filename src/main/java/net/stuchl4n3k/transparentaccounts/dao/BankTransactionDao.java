package net.stuchl4n3k.transparentaccounts.dao;

import java.util.List;
import net.stuchl4n3k.transparentaccounts.domain.Bank;
import net.stuchl4n3k.transparentaccounts.domain.BankAccount;
import net.stuchl4n3k.transparentaccounts.domain.BankTransaction;

/**
 * @author petr.stuchlik
 * @since 2017-06-25
 */
public interface BankTransactionDao {

    Bank getSupportedBank();

    List<BankTransaction> loadAllByAccount(BankAccount account);

}
