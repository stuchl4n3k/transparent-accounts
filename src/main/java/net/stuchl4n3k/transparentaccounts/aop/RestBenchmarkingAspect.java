package net.stuchl4n3k.transparentaccounts.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * An AspectJ advice for benchmarking REST controller methods.
 *
 * @author petr.stuchlik
 * @since 2017-07-14
 */
@Aspect
@Component
public class RestBenchmarkingAspect {

    private static final Logger LOG = LoggerFactory.getLogger(RestBenchmarkingAspect.class);

    @Pointcut("execution(public * *(..))")
    public void anyPublicMethod() {}

    @Pointcut("within(net.stuchl4n3k.transparentaccounts.integration.rest..*)")
    private void inRestControllerLayer() {}

    @Pointcut("@target(org.springframework.web.bind.annotation.RestController)")
    public void withRestControllerAnnotation() {}

    @Pointcut("inRestControllerLayer() && withRestControllerAnnotation()")
    public void inRestController() {}

    @Around("anyPublicMethod() && inRestController()")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
        long t = System.currentTimeMillis();
        Object retVal = pjp.proceed();
        t = System.currentTimeMillis() - t;

        LOG.info("Execution of {} took {} ms.", pjp.getSignature().toShortString(), t);
        return retVal;
    }

}
