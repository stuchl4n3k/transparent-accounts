function LoadingControl($el) {
    $(document).ajaxStart(function () {
        $el.show();
    }).ajaxComplete(function () {
        $el.hide();
    });
}

function TxTableControl($el) {
    var formatString = function (value) {
        return value || '';
    };

    var formatDate = function (timestamp) {
        return new Date(timestamp).toLocaleDateString();
    };

    var formatAmount = function (amount) {
        return Number(amount)
            .toFixed(2)
            .replace(/(\d)(?=(\d{3})+\.)/g, '$1&nbsp;') + ",-";
    };

    var formatAccount = function (account) {
        var formatted = [account.number, account.name]
            .filter(function (n) {
                return n !== null;
            })
            .join(', ');
        if (formatted && account.statementUrl) {
            formatted = '<a href="' + account.statementUrl + '">' + formatted + '</a>';
        }
        return formatted;
    };

    var transactionToRow = function (transaction) {
        return '<tr>' +
            '<td>' + formatDate(transaction.dateCreated) + '</td>' +
            '<td>' + formatAccount(transaction.from) + '</td>' +
            '<td>' + formatAccount(transaction.to) + '</td>' +
            '<td class="' + (transaction.amount > 0 ? 'bg-success' : 'bg-danger') + '">' + formatAmount(transaction.amount) + '</td>' +
            '<td>' + formatString(transaction.variableSymbol) + '</td>' +
            '<td>' + formatString(transaction.constantSymbol) + '</td>' +
            '<td>' + formatString(transaction.specificSymbol) + '</td>' +
            '<td>' + formatString(transaction.description) + '</td>' +
            '<td>' + formatString(transaction.messageForRecipient) + '</td>' +
            '</tr>';
    };

    // Load transactions.
    $.get('/api/v1/transactions', function (data) {
        if (!data) {
            console.log("No data!");
            return;
        }

        data.forEach(function (transaction) {
            $el.append(transactionToRow(transaction));
        });
    });
}

function TxTableFilterControl($el, $tableEl) {
    // Fulltext search in the table.
    var filterTransactions = function (query) {
        console.log(query)
        var $rows = $tableEl.find("tr");
        if (!query) {
            $rows.show();
            return;
        }
        var keywords = query.toUpperCase().split(' ');

        $rows.hide();
        $rows.filter(function () {
            var $row = $(this);
            var match = true;
            keywords.forEach(function (keyword) {
                if ($row.text().toUpperCase().indexOf(keyword) < 0) {
                    match = false;
                    return false;
                }
            });
            return match;
        }).show();
    };

    $(document).on("focusout", $el, function() {
        filterTransactions($el.val());
    });
    $el.keypress(function (e) {
        if (e.which === 13) filterTransactions($el.val());
    });
}

$(function () {
    var $loadingEl = $('#loading');
    var $txTableEl = $('#transactionsTable').find('> tbody');
    var $txTableFilterEl = $('#transactionsTableFilter');

    var loadingContol = new LoadingControl($loadingEl);
    var txTableControl = new TxTableControl($txTableEl);
    var txTableFilterControl = new TxTableFilterControl($txTableFilterEl, $txTableEl);
});
