# TRANSPARENT ACCOUNTS

**This is simple Boot application providing common interface with access to transactions on transparent accounts of Czech political parties.**

## Feature set

 - Parsing an online list of transparent accounts (https://registrace.udhpsh.cz/seznam/ofzvu/2017ps).
 - Parsing transactions on each statements feed (communists PDF feeds are not supported yet).
 - Feeds of following banks are currently supported: KB, MONETA, CSAS and FIO.
 - Full-text filtering of transactions using multiple keywords.
 - REST API for transactions at `/api/v1/transactions`.
 
## Technology stack and tooling

 - Spring Boot
 - Lombok
 - Thymeleaf
 - Jerry (Jodd/Lagarto)
 - Bootstrap + jQuery
 - Maven
 - Docker
 - Google Cloud Container Engine